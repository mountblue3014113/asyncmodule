const fs = require("fs");

const createDelSimultaneously = ()=>{
fs.mkdir("jsonFiles", (err) => {
  if (err) {
    return console.error(err);
  } else {
    console.log("Directory created !");
    let count = 1;
    function rec(count) {
      if (count < 5) {
        fs.writeFile(`./jsonFiles/file${count}.json`, "", (err) => {
          if (err) {
            return console.error(err);
          } else {
            console.log(`file${count} created successfully!`);
            fs.unlink(`./jsonFiles/file${count}.json`, (err) => {
              if (err) {
                return console.error(err);
              } else {
                console.log(`file${count} deleted successfully!`);
                rec(++count);
              }
            });
          }
        });
      }
    }
    rec(count);
  }
});
}
module.exports = createDelSimultaneously;