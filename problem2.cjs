const { log } = require("console");
const fs = require("fs");
const path = require("path");

const readFile = ()=>{
    fs.readFile("../lipsum_1.txt","utf-8",(err,data)=>{
        if(err){
            console.error(err);
        }else{
            console.log("lipsum.txt file read !")
            fs.writeFile("../output/upperCase.txt",data.toUpperCase(),(err)=>{
                if(err){
                    console.error(err);
                }else{
                    console.log("Written in upperCase file !");
                    
                    fs.appendFile("../output/filenames.txt",'upperCase.txt\n',(err)=>{
                        if(err){
                            console.error(err);
                        }else{
                            console.log("stored name of upperCase file !");
                            fs.readFile("../output/upperCase.txt","utf-8",(err,data)=>{
                                if(err){
                                    console.error(err);
                                }else{
                                    console.log("upperCase file read !")
                                    let splittedFile = data.toLowerCase().split('.');
                                    let lines = [];
                                    for(let index = 0; index < splittedFile.length; index++) {
                                          lines.push(splittedFile[index].trim());
                                          }
                                     let newText = lines.join('.\r\n'); 
                                    fs.writeFile("../output/lowerCaseSplitted.txt",newText,(err)=>{
                                        if(err){
                                            console.error(err);
                                        }else{
                                            console.log("Written in lowerCaseSplitted file !");
                                            fs.appendFile("../output/filenames.txt",' lowerCaseSplitted.txt'.trim(),(err)=>{
                                                if(err){
                                                    console.error(err);
                                                }else{
                                                    console.log("stored name of lowerCaseSplitted file !");
                                                    fs.readFile("../output/lowerCaseSplitted.txt","utf-8",(err,data)=>{
                                                        if(err){
                                                            console.error(err);
                                                        }else{
                                                            console.log("lowerCaseSplitted file read !")
                                                            let newLine = data.split('\n');
                                                            let sortedFile = newLine.sort().join('\n');
                                                            fs.writeFile("../output/sorted.txt",sortedFile,(err)=>{
                                                                if(err){
                                                                    console.error(err);
                                                                }else{
                                                                    console.log("sorted file written !");
                                                                    fs.appendFile("../output/filenames.txt",' \nsorted.txt',(err)=>{
                                                                        if(err){
                                                                            console.error(err);
                                                                        }else{
                                                                            console.log("stored name of sorted file !");
                                                                            fs.readFile("../output/filenames.txt","utf-8",(err,data)=>{
                                                                                if(err){
                                                                                    console.error(err);
                                                                                }else{
                                                                                    console.log("filenames.txt file read !");
                                                                                    let eachFile = data.split('\n');
                                                                                    console.log(`filenames has ${eachFile[0]} !`);
                                                                                    fs.unlink(`../output/${eachFile[0]}`,(err)=>{
                                                                                        if(err){
                                                                                            console.error(err);
                                                                                        }else{
                                                                                            console.log(`${eachFile[0]} deleted`);
                                                                                            console.log(`filenames has ${eachFile[1]} !`);
                                                                                            fs.unlink(`../output/${eachFile[1].trim()}`,(err)=>{
                                                                                                if(err){
                                                                                                    console.error(err);
                                                                                                }else{
                                                                                                    console.log(`${eachFile[1]} deleted`);
                                                                                                    console.log(`filenames has ${eachFile[2]} !`);
                                                                                                    fs.unlink(`../output/${eachFile[2].trim()}`,(err)=>{
                                                                                                        if(err){
                                                                                                            console.error(err);
                                                                                                        }else{
                                                                                                            console.log(`${eachFile[2]} deleted`);
                                                                                                            
                                                                                                        }})
                                                                                                }})
                                                                                                
                                                                                                
                                                                                        }
                                                                                    })
                                                                                 
                                                                                }
                                                                            })
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                                
                                
                            })

                        }
                    })
                }
            })
        }
    });

}
module.exports = readFile;